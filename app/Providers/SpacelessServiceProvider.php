<?php
namespace App\Providers;

use Blade;
use Illuminate\Support\ServiceProvider;

class SpacelessBladeProvider extends ServiceProvider
{
    public function boot()
    {
        Blade::extend(function($view) {
            $pattern = $this->createPlainMatcher('spaceless');
            return preg_replace($pattern, '<?php ob_start() ?>', $view);
        });

        Blade::extend(function($view) {
            $pattern = $this->createPlainMatcher('endspaceless');
            return preg_replace($pattern, "<?php echo addslashes(preg_replace('/>\\s+</', '><', ob_get_clean())); ?>", $view);
        });
    }

    public function createPlainMatcher($function)
    {
        return '/(?<!\w)(\s*)@'.$function.'(\s*)/';
    }

    public function register(){}
}